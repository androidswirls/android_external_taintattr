#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <sys/time.h>
#include <sys/types.h>
#include <assert.h>
#include <dirent.h>

#include <sys/syscall.h>
#include <unistd.h>

#define __NR_SYSCALL_BASE       0
#define SYS_gettaint __NR_SYSCALL_BASE+376
#define SYS_settaint __NR_SYSCALL_BASE+377

// ioctls
#define SWIRLS_DEVICE	"/dev/taintctrl"


#define SWIRLS_IOC_MAGIC	'G'
#define SWIRLS_SYSTEM	_IO(SWIRLS_IOC_MAGIC, 0)
#define SWIRLS_READ		_IOR(SWIRLS_IOC_MAGIC, 1, char *)
#define SWIRLS_WRITE		_IOW(SWIRLS_IOC_MAGIC, 2, char *)
#define SWIRLS_GETTAINT	_IOR(SWIRLS_IOC_MAGIC, 3, int *)
#define SWIRLS_SETTAINT	_IOW(SWIRLS_IOC_MAGIC, 4, int *)
#define SWIRLS_CAPSULE	_IOW(SWIRLS_IOC_MAGIC, 5, struct capsule_t *)
#define SWIRLS_CONTEXT_ADD	_IOW(SWIRLS_IOC_MAGIC, 6, struct context_t *)
#define SWIRLS_POLICY	_IOW(SWIRLS_IOC_MAGIC, 7, struct policy_t *)
#define SWIRLS_EVAL_POLICY	_IOWR(SWIRLS_IOC_MAGIC, 8, int *)
#define SWIRLS_SET_NEXT_TAG	_IOW(SWIRLS_IOC_MAGIC, 9, struct object_t *)
#define SWIRLS_GET_NEXT_TAG	_IOWR(SWIRLS_IOC_MAGIC, 10, struct object_t *)
#define SWIRLS_GET_SOCKET_TAG	_IOWR(SWIRLS_IOC_MAGIC, 11, struct object_t *)
#define SWIRLS_SET_SOCKET_TAG	_IOW(SWIRLS_IOC_MAGIC, 12, struct object_t *)
#define SWIRLS_WHITELIST_PID	_IOW(SWIRLS_IOC_MAGIC, 13, int *)
#define SWIRLS_CONTEXT_TIMEFENCE_ENTER	_IOW(SWIRLS_IOC_MAGIC, 14, struct context_t *)
#define SWIRLS_CONTEXT_TIMEFENCE_EXIT	_IOW(SWIRLS_IOC_MAGIC, 15, struct context_t *)
#define SWIRLS_CONTEXT_GEOFENCE_ENTER	_IOW(SWIRLS_IOC_MAGIC, 16, struct context_t *)
#define SWIRLS_CONTEXT_GEOFENCE_EXIT	_IOW(SWIRLS_IOC_MAGIC, 17, struct context_t *)
#define SWIRLS_SET_APP_TAG	_IOW(SWIRLS_IOC_MAGIC, 18, struct object_t_u *)
#define SWIRLS_GET_APP_TAG	_IOWR(SWIRLS_IOC_MAGIC, 19, struct object_t_u *)


const char * whitelist[] = {
	"system_server",
	"servicemanager",
	"vold",
	"netd",
	"debuggerd",
	"surfaceflinger",
	"zygote",
	"drmserver",
	"mediaserver",
	"dbus-daemon",
	"installd",
	"keystore",
	"glgps",
	"adbd",
	"bluetoothd",
	"com.android.inputmethod.latin",
	"com.android.providers.calendar",
	"com.android.nfc",

};

enum TAG_RESPONSE {
  TAG_NONEXISTANT = 0,
  TAG_BLOCK,
  TAG_ALLOW,
  TAG_ALLOW_LOG,
  TAG_UPDATE
};

enum OOC_ACTION {
	OOC_BLOCK = 0, // Block execution or transfer of information
	OOC_ALLOW, // Allow execution or transfer of information
	OOC_ALLOW_LOG, // Allow + log everything that happen
	OOC_SUPPRIM, // Supprim data while leaving the context
	OOC_ENCRYPT
};

enum GEOFENCE_STATUS {
	INSIDE_GEOFENCE,
	OUTSIDE_GEOFENCE
};

enum TIMEFENCE_STATUS {
	INSIDE_TIMEFENCE,
	OUTSIDE_TIMEFENCE
};

struct object_t {
	int tag;
	int64_t idcontext;
	char *name;
};

struct capsule_t {
	int64_t idcapsule;
	char *name;
	int tag;
	int version;
};

struct policy_t {
	int64_t from;
	int64_t to;
	enum TAG_RESPONSE action;
};

struct context_t {
	int64_t idcontext;
	char *type;
	enum GEOFENCE_STATUS geoFenceStatus;
	enum TIMEFENCE_STATUS timeFenceStatus;
	enum OOC_ACTION outofcontextaction;
};

void print_help()
{
	fprintf(stderr, "Usage: taintattr ACTION PARAM1 [PARAM2]\n");
	fprintf(stderr, "Get and set tags to process dynamically\n\
			Show the policy between two tags\n\n");
	fprintf(stderr, "ACTION is one if the following:\n");
	fprintf(stderr, "  g		Get process taint, requieres a PID as PARAM1\n");
	fprintf(stderr, "  s		Set process taint, requieres a PID as PARAM1, and a tag as PARAM2\n");
	fprintf(stderr, "  p		Print the policy inforced between tags PARAM1 and PARAM2 in context PARAM3\n");
	fprintf(stderr, "  n		Set the next application PARAM1 tag to PARAM2\n");
	fprintf(stderr, "  w		Whitelist processes\n");
}

char* basename(char *name)
{
	char *cp = strrchr(name, '/');
	if (cp)
		return cp + 1;
	return name;
}

int find_pid(const char* name)
{
	DIR *dir;
	struct dirent* ent;
	char* endptr, *bbuf;
	char buf[512];
	int lpid;

	if (!(dir = opendir("/proc"))) {
		perror("can't open /proc");
		return -1;
	}

	while((ent = readdir(dir)) != NULL) {
		// * if endptr is not a null character, the directory is not
		// * entirely numeric, so ignore it
		lpid = strtol(ent->d_name, &endptr, 10);
		if (*endptr != '\0') {
			continue;
		}

		// try to open the cmdline file
		snprintf(buf, sizeof(buf), "/proc/%d/cmdline", lpid);
		FILE* fp = fopen(buf, "r");

		if (fp) {
			if (fgets(buf, sizeof(buf), fp) != NULL) {
				// check the first token in the file, the program name
				// char* first = strtok(buf, " ");
				// Go to the last / to get the basename
				bbuf = basename(buf);
				//printf("Examination of process %s\n", buf);
				if (!strncmp(bbuf, name, strlen(name))) {
					fclose(fp);
					closedir(dir);
					return lpid;
				}
			}
			fclose(fp);
		}

	}

	closedir(dir);
	return -1;
}

int update_pid(int fd, int pid){
	if(ioctl(fd, SWIRLS_WHITELIST_PID, &pid) < 0)
		perror("System ready ioctl");

	return 0;
}


int whitelist_system_process(int fd)
{
	int n;
	int pid;

	for (n = 0; n < sizeof(whitelist) / sizeof(char *); n++){
		printf("Removing any taint for %s\n", whitelist[n]);
		pid = find_pid(whitelist[n]);
		if (pid != -1)
			update_pid(fd, pid);
	}

	return 0;
}

int main(int argc, char ** argv)
{
	int ret;
	int fd;

	// Opening amnidroid char device
	if ((fd = open(SWIRLS_DEVICE, O_RDWR)) < 0) {
		perror("open");
		return EXIT_FAILURE;
	}

	if ( argc < 3 || argc > 5 )
		goto error;

	switch (argv[1][0])
	{

//		printf("Parameters given: ");
//		for (i=0; i < argc; i++)
//			printf(" * %s", argv[i]);
//		printf("\n");

		// gettaint for a process
		case 'g':
			if ( argc != 3 )
				goto error;
			ret = syscall(SYS_gettaint, atoi(argv[2]));
			printf("errno: %i\n", errno);
			printf("gettaint on process %s = %i\n", argv[2], ret);
			break;
			// settaint for a process
		case 's':
			if ( argc != 4 )
				goto error;
			ret = syscall(SYS_settaint, atoi(argv[2]), atoi(argv[3]));
			printf("errno: %i\n", errno);
			printf("settaint on process %s = %i\n", argv[2], ret);
			break;
			// print policy between two taints
		case 'p':
			if ( argc != 5 )
				goto error;
			int param[3];
			int i;
			/*
			for (i=2; i<argc; i++)
				param[i-2] = atoi(argv[i]);
*/
			param[0] = atoi(argv[2]);
			param[1] = atoi(argv[3]);
			param[2] = atoi(argv[4]);


			if(ioctl(fd, SWIRLS_EVAL_POLICY, param) < 0){
				perror("Eval policy IOCTL");
				goto error;
			}

			printf("policy between taint %d and %d: %d", atoi(argv[2]), atoi(argv[3]), param[0]);
			switch (param[0])
			{
				case TAG_NONEXISTANT:
					printf(" TAG_NONEXISTANT\n");
					break;
				case TAG_BLOCK:
					printf(" TAG_BLOCK\n");
					break;
				case TAG_ALLOW:
					printf(" TAG_ALLOW\n");
					break;
				case TAG_ALLOW_LOG:
					printf(" TAG_ALLOW_LOG\n");
					break;
				case TAG_UPDATE:
					printf(" TAG_UPDATE\n");
					break;
				default:
					printf(" ERROR: policy action not defined\n");
			}
			break;
		case 'n':
			if (argc != 4)
				goto error;
			int tag = atoi(argv[3]);

			struct object_t next;
			next.tag = tag;
			next.name = strdup(argv[2]);

			sprintf(stdout, "setNextTag tag:%d - name:%s", next.tag, next.name);
			if(ioctl(fd, SWIRLS_SET_NEXT_TAG, &next) < 0){
				perror("Set next tag IOCTL fail");
				goto error;
			}
			break;
		case 'w': // whitelist process
			whitelist_system_process(fd);
			break;
		default:
			goto error;
	}

	close(fd);
//	if (! close(fd))
//		perror("close");

	return EXIT_SUCCESS;

error:
	if (! close(fd))
		perror("close");
	print_help();
	return EXIT_FAILURE;

	/*

	   if (!strcmp (argv[1], "g")){
	   if ( argc != 2 ) {
	   }
	   ret = syscall(SYS_gettaint, atoi(argv[2]));
	   printf("errno: %i\n", errno);
	   printf("gettaint on process %s = %i\n", argv[2], ret);
	   }
	   if (!strcmp (argv[1], "s")){
	   ret = syscall(SYS_settaint, atoi(argv[2]), atoi(argv[3]));
	   printf("errno: %i\n", errno);
	   printf("settaint on process %s = %i\n", argv[2], ret);
	   }

	   return 0;
	   */

}
