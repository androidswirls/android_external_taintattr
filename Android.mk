LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_SRC_FILES:= taintattr.c
LOCAL_MODULE := taintattr
LOCAL_STATIC_LIBRARIES := libcutils libc
LOCAL_C_INCLUDES := external/sqlite/dist
LOCAL_SHARED_LIBRARIES := libsqlite
include $(BUILD_EXECUTABLE)
